<?php
function seculoAno( $pAno = 1 ){
    if( is_integer($pAno) ){
        $seculo     = 1;
        $anoLimite  = 1;
        for( $ano = 1; $ano <= intval(date('Y')); $ano = $ano + 100){
            $anoLimite = $ano + 99;
            if( ($pAno >= $ano) && ($pAno <= $anoLimite) ) {
                return $seculo;
            }
            $seculo++;
        }
    }
    return 0;
}

echo "Século: ".seculoAno(1905);
echo "<br/>";
echo "Século: ".seculoAno(1700);