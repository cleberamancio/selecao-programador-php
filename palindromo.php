<?php
function PALINDROMO($STR)
{
    if( strlen($STR) <= 1){
        return 'true';
    }

    if( strrev($STR) == $STR ){
        return 'true';
    }

    return 'false';
}

echo "A string enviada é um palindromo? ".PALINDROMO("aabaa");
echo "<br>";
echo "A string enviada é um palindromo? ".PALINDROMO("a");
echo "<br>";
echo "A string enviada é um palindromo? ".PALINDROMO("abac");
