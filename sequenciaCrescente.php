<?php
function sequenciaCrescente($array){

    $maiorNumero        = 0;
    $menorNumero        = 0;
    $indexMaiorNumero   = 0;
    $count              = count($array);
    $i                  = 1;
    $ocorrencia         = 0;
    $q = 0;
    foreach( $array as $index => $num ){
        if( $num > $maiorNumero  )
        {
            $maiorNumero        = $num;
            $indexMaiorNumero   = $index;
        } 
    }

    $sequance   = 0;
    $erro       = 0;
    foreach( $array as $index => $num ){
        if( $num > $sequance ){
            $sequance = $num;
            $erro++;
        }
            
    }


        return "";
}

echo "É possivel uma sequencia [1, 3, 2, 1]: ".sequenciaCrescente([1, 3, 2, 1]); //false
echo "<br>";
echo "É possivel uma sequencia [1, 3, 2]: ".sequenciaCrescente([1, 3, 2]); //true
echo "<br>";
echo "É possivel uma sequencia [1, 2, 1, 2]: ".sequenciaCrescente([1, 2, 1, 2]); //false
echo "<br>";
echo "É possivel uma sequencia [3, 6, 5, 8, 10, 20, 15]: ".sequenciaCrescente([3, 6, 5, 8, 10, 20, 15]); //false
echo "<br>";
echo "É possivel uma sequencia [1, 1, 2, 3, 4, 4]: ".sequenciaCrescente([1, 1, 2, 3, 4, 4]); //false
echo "<br>";
echo "É possivel uma sequencia [1, 4, 10, 4, 2]: ".sequenciaCrescente([1, 4, 10, 4, 2]); //false
echo "<br>";
echo "É possivel uma sequencia [10, 1, 2, 3, 4, 5]: ".sequenciaCrescente([10, 1, 2, 3, 4, 5]); //true
echo "<br>";
echo "É possivel uma sequencia [1, 1, 1, 2, 3]: ".sequenciaCrescente([1, 1, 1, 2, 3]); //false
echo "<br>";
echo "É possivel uma sequencia [0, -2, 5, 6]: ".sequenciaCrescente([0, -2, 5, 6]); //true
echo "<br>";
echo "É possivel uma sequencia [1, 2, 3, 4, 5, 3, 5, 6]: ".sequenciaCrescente([1, 2, 3, 4, 5, 3, 5, 6]); //false
echo "<br>";
echo "É possivel uma sequencia [40, 50, 60, 10, 20, 30]: ".sequenciaCrescente([40, 50, 60, 10, 20, 30]); //false
echo "<br>";
echo "É possivel uma sequencia [1, 1]: ".sequenciaCrescente([1, 1]); //true
echo "<br>";
echo "É possivel uma sequencia [1, 2, 5, 3, 5]: ".sequenciaCrescente([1, 2, 5, 3, 5]); //true
echo "<br>";
echo "É possivel uma sequencia [1, 2, 5, 5, 5]: ".sequenciaCrescente([1, 2, 5, 5, 5]); //false
echo "<br>";
echo "É possivel uma sequencia [10, 1, 2, 3, 4, 5, 6, 1]: ".sequenciaCrescente([10, 1, 2, 3, 4, 5, 6, 1]); //false
echo "<br>";
echo "É possivel uma sequencia [1, 2, 3, 4, 3, 6]: ".sequenciaCrescente([1, 2, 3, 4, 3, 6]); //true
echo "<br>";
echo "É possivel uma sequencia [1, 2, 3, 4, 99, 5, 6]: ".sequenciaCrescente([1, 2, 3, 4, 99, 5, 6]); //true
echo "<br>";
echo "É possivel uma sequencia [123, -17, -5, 1, 2, 3, 12, 43, 45]: ".sequenciaCrescente([123, -17, -5, 1, 2, 3, 12, 43, 45]); //true
echo "<br>";
echo "É possivel uma sequencia [3, 5, 67, 98, 3]: ".sequenciaCrescente([3, 5, 67, 98, 3]); //true
